{
    "name": "Nicolai",
    "comments": {
        {
            "comment": "Dette er min første kommentar",
            "date": "20-02-2020"
        },
        {
            "comment": "Dette er min anden kommentar",
            "date": "21-02-2020"
        },
        {
            "comment": "Dette er min tredje kommentar",
            "date": "22-02-2020"
        }
    }
}


{
    "name": "Nicolai",
    "comments": [
        commentId1,
        commentId2,
        commentId3
    ]
}

{
    "_id": commentId1,
    "comment": "Dette er min første kommentar",
    "date": "20-02-2020"
}

{
    "_id": commentId2,
    "comment": "Dette er min anden kommentar",
    "date": "21-02-2020"
}

{
    "_id": commentId3,
    "comment": "Dette er min tredje kommentar",
    "date": "22-02-2020"
}




{
    "name": "Nicolai",
    "_id": nicolaiId
}

{
    "_id": commentId1,
    "comment": "Dette er min første kommentar",
    "date": "20-02-2020",
    "author": nicolaiId
}

{
    "_id": commentId2,
    "comment": "Dette er min anden kommentar",
    "date": "21-02-2020",
    "author": nicolaiId
}

{
    "_id": commentId3,
    "comment": "Dette er min tredje kommentar",
    "date": "22-02-2020",
    "author": nicolaiId
}