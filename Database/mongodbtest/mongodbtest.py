from pymongo import MongoClient
client = MongoClient()

db = client.pymongoTestDB


pymongoTest = {
     "title": "Working With, MongoDB, and JSON Data in Python",
     "author": "Nicolai",
     "keypoints": [
         "No scheme",
         "Unstructered data"
     ],
     "Age": 33,
     "url": "https://nicolaisparvath.dk/"
}
pymongoTest2 = {
     "title": "Blog post work in progress",
     "author": "Nicolai",
     "keypoints": [
         "Scalability",
         "Fast"
     ],
     "Age": 33,
     "url": "https://nicolaisparvath.dk/"
}
authors = db.authors
authors.insert_many([pymongoTest,pymongoTest2])

import pprint
for doc in authors.find():
    pprint.pprint(doc)