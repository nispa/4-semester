from .db import db
from flask_bcrypt import generate_password_hash, check_password_hash

class Machine(db.Document):
    name = db.StringField(required=True, unique=True)
    type = db.StringField(required=True)
    parts = db.ListField(db.StringField(), required=True)
    added_by = db.ReferenceField('User')

class User(db.Document):
    email = db.EmailField(required=True, unique=True)
    password = db.StringField(required=True, min_length=6)
    machines = db.ListField(db.ReferenceField('Machine', reverse_delete_rule=db.PULL))

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        return check_password_hash(self.password, password)

User.register_delete_rule(Machine, 'added_by', db.CASCADE)