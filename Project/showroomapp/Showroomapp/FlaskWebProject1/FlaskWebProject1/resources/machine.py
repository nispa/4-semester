from flask import Response, request
from FlaskWebProject1.data.models import Machine, User
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

class MachinesApi(Resource):
    def get(self):
        machines = Machine.objects().to_json()
        return Resonse(machines, mimetype="application/json", status=200)

    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        body = request.get_json()
        user = User.objects.get(id=user_id)
        machine = Machine(**body, added_by=user)
        machine.save()
        user.update(push__machines=machine)
        user.save()
        id = machine.id
        return {'id': str(id)}, 200

class MachineApi(Resource):
    def get(self, id):
        machines = Machine.objects.get(id=id).to_json()
        return Response(machines, mimetype="application/json", status=200)

    @jwt_required
    def put(self, id):
        user_id = get_jwt_identity()
        machine = Machine.objects.get(id=id, added_by=user_id)
        body = request.get_json()
        machine.update(**body)
        return '', 200

    @jwt_required
    def delete(self, id):
        user_id = get_jwt_identity()
        machine = Machine.objects.get(id=id, added_by=user_id)
        machine.delete()
        return '', 200