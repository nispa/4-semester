from .machine import MachinesApi, MachineApi
from .auth import SignupApi, LoginApi

def initialize_routes(api):
    api.add_resource(MachinesApi, '/api/machine')
    api.add_resource(MachineApi, '/api/machine/<id>')
    api.add_resource(SignupApi, '/api/auth/signup')
    api.add_resource(LoginApi, '/api/auth/login')
