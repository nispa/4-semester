"""
Routes and views for the flask application.
"""
from datetime import datetime
from FlaskWebProject1.data.db import initialize_db
from FlaskWebProject1.data.models import Machine
from FlaskWebProject1 import app
from flask_restful import Api
from FlaskWebProject1.resources.routes import initialize_routes


api = Api(app)

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/showroom'
}
initialize_db(app)

initialize_routes(api)

@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
